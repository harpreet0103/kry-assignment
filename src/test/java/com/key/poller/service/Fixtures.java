package com.key.poller.service;

import com.kry.poller.entity.UserEntity;
import com.kry.poller.entity.UserServiceEntity;

public class Fixtures {

    public static final UserEntity newUser() {
        return UserEntity.builder()
            .userName("user1")
            .password("password")
            .role("USER")
            .firstName("firstname")
            .lastName("lastname")
            .build();
    }

    public static final UserServiceEntity newUserService() {
        return UserServiceEntity.builder()
            .name("service")
            .url("test")
            .build();
    }
}
