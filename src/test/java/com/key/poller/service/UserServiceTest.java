package com.key.poller.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.kry.poller.PollerApplication;
import com.kry.poller.entity.UserEntity;
import com.kry.poller.entity.UserServiceEntity;
import com.kry.poller.service.UserServiceHandler;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(classes = PollerApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
public class UserServiceTest {

    @Autowired
    private UserServiceHandler userServiceHandler;

    @Test
    public void testCreateUser() {
        final UserEntity userEntity = Fixtures.newUser();

        userServiceHandler.saveUser(userEntity);

        assertNotNull(userEntity.getId(), "Id must be auto generated and not null");
    }

    @Test
    public void testCreateUserService() {
        final UserEntity userEntity = userServiceHandler.findUserById(1L).orElseThrow();
        final UserServiceEntity userServiceEntity = Fixtures.newUserService();
        userServiceEntity.setUser(userEntity);

        userServiceHandler.saveUserService(userServiceEntity);

        assertNotNull(userServiceEntity.getId(), "Id must be auto generated and not null");
    }

    @Test
    public void testUpdateUserService() {
        final UserServiceEntity userServiceEntity = userServiceHandler.findById(1L).orElseThrow();
        userServiceEntity.setName("Updated name");

        userServiceHandler.saveUserService(userServiceEntity);

        assertEquals("Updated name", userServiceEntity.getName(), "Name must match");
    }

    @Test
    public void testDeleteUserService() {
        final UserServiceEntity userServiceEntity = userServiceHandler.findById(1L).orElseThrow();

        userServiceHandler.deleteUserService(userServiceEntity);
        List<UserServiceEntity> services = userServiceHandler.getAllEnabledUserServices();

        Assertions.assertTrue(services.size() ==  0, "There should be no enabled services");
    }
}
