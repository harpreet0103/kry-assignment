package com.key.poller.util;

import static com.kry.poller.service.util.Utils.isValidUrl;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

public class UrlValidatorTest {

    @ParameterizedTest
    @CsvSource({
        "http:/localhost:8080/check-status", "http/localhost:8080/status-check", "http://192.65..1", "its_a_url"
    })
    void validation_must_fail_if_url_is_invalid(final String url) {
        final boolean validationResult = isValidUrl(url);

        assertFalse(validationResult, "Validation must fail for " + url);
    }

    @ParameterizedTest
    @CsvSource({
        "http://localhost:8080/check-status", "http://192.65.3.1"
    })
    void validation_must_succeed_if_url_is_valid(final String url) {
        final boolean validationResult = isValidUrl(url);

        assertTrue(validationResult, "Validation must succeed for " + url);
    }

    @ParameterizedTest
    @CsvSource({
        "tcp://localhost:8080/check-status", "ftp://192.65.3.1"
    })
    void validation_must_fail_for_schemes_other_than_HTTP_AND_HTTPS(final String url) {
        final boolean validationResult = isValidUrl(url);

        assertFalse(validationResult, "Validation must fail for " + url);
    }
}
