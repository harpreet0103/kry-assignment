let App = {
	rootContext: "/",
	userServiceTableId: "#table-user-service",
	userServiceTable: {
		column: {
			"serviceName": 0,
			"url": 1,
			"status": 2,
			"createdOn": 3,
			"lastUpdated": 4,
            "serviceId": 5,
			"index": 6
		}
	},
	hideModal: () => {
		 $(".modal").modal("hide");
	},
	init: () => {
    },
    clearTable: (id) => {
		if ($.fn.DataTable.isDataTable(id)) {
			  $(id).DataTable().clear().destroy();
			}
		$(id + " thead").remove();
		$(id + " tbody").remove();
		//$(id).hide();
	},
    Index: {
    	init: () => {
            $("#err-msg").hide();
    		
    		$("#add-user-service").off("click");
    		$("#add-user-service").on("click", (event) => {
    			App.Index.addUserService(event);
    		});
    		
    		$("#update-user-service").off("click");
    		$("#update-user-service").on("click", (event) => {
    			App.Index.updateUserService();
    		});
    		
    		$("#delete-user-service").off("click");
    		$("#delete-user-service").on("click", (event) => {
    			App.Index.deleteUserService();
    		});
    		
    		$(document).ajaxError(function( event, jqxhr, settings, error ) {
			  if(jqxhr.responseJSON) {
				  $("#err-msg").text(jqxhr.responseJSON.message);
				  $("#err-msg").show();
			  }
    		});
    		
    		$(document).ajaxSend(function( event, jqxhr, settings, error ) {
    			$("#err-msg").hide();
    		});
    		
    		$('#addNewUserServiceModal').on('hidden.bs.modal', function (e) {
			  $("#user-service-form .clear-value").each(function() {
                $(this).val('');
              })
    		});
    		
    		App.Index.populateUserServiceTable();
    	},
    	addUserService: (event) => {
    		let userService = {
				name: $("#service-name").val(),
				url: $("#service-url").val()
    		}
    		$.ajax({
    			  url: App.rootContext + "user-service/",
    			  data: JSON.stringify(userService),
    			  contentType : 'application/json',
    			  type : 'POST'
    			}).done(function() {
					App.Index.populateUserServiceTable();
    			});
    		App.hideModal();
    	},
    	deleteUserService: () => {
    		$.ajax({
  			  url: App.rootContext + "user-service/" + $("#delete-user-service-id").val(),
  			  type : 'DELETE'
  			}).done(function() {
                App.Index.populateUserServiceTable();
  			});
    		App.hideModal();
    	},
    	updateUserService: () => {
    		let serviceId = $("#update-user-service-id").val();
    		let userService = {
				id: serviceId,
    			name: $("#update-user-service-name").val(),
				url: $("#update-user-service-url").val()
    		}
    		$.ajax({
    			  url: App.rootContext + "user-service/" + serviceId,
    			  data: JSON.stringify(userService),
    			  contentType : 'application/json',
    			  type : 'PUT'
    			}).done(function() {
					App.Index.populateUserServiceTable();
    			});
    		App.hideModal();
    	},
    	populateUserServiceTable: (callback) => {
    		$.getJSON(App.rootContext + "user-service/list")
			.done((data) => {
				App.clearTable(App.userServiceTableId);
				let rows = [];
				
				if(data && data.length > 0) {
					App.userServices = data;
					data.forEach((userService, index) => {
						let row = ['', '', '', '', '', '',  index];
						row[0] = userService.name;
						row[1] = userService.url;
						row[2] = userService.status;
						row[3] = userService.createdOn;
						row[4] = userService.lastUpdated;
						row[5] = userService.id;
						rows.push(row);
					});
					
				}
				
				let table = $(App.userServiceTableId).DataTable({
					data: rows,
					ordering: false,
					destroy: true,
					columns: [
						{ title: "Service Name" },
			            { title: "Url" },
			            { title: "Status" },
			            { title: "Added On" },
			            { title: "Last Updated" },
			            {
			                title: "Action",
			                data: null,
			                className: "center",
			                width: "15%",
			                defaultContent: `
			                	<button type="button"  class="btn btn-info custom-width" data-action="edit">Edit</button>
                                <button type="button"  class="btn btn-danger custom-width" data-action="delete">Remove</button>`
			            }
					]
				});
				$(App.userServiceTableId.concat(" tbody")).off("click", "tr");
				$(App.userServiceTableId.concat(" tbody")).on("click", "tr", (event) => {
					let element = $(event.target);
					let rowElement = $(event.currentTarget);
					let action = element.data("action");
					let rowData = table.row(rowElement).data();
					switch(action){
                        case "edit" :
                            $("#update-user-service-name").val(rowData[App.userServiceTable.column.serviceName]);
                            $("#update-user-service-url").val(rowData[App.userServiceTable.column.url]);
                            $("#update-user-service-id").val(rowData[App.userServiceTable.column.serviceId]);
                            $("#updateUserServiceModal").modal("show");
                            break;
                        case "delete" :
                            $("#delete-user-service-text").text(rowData[App.userServiceTable.column.serviceName]);
                            $("#delete-user-service-id").val(rowData[App.userServiceTable.column.serviceId]);
                            $("#deleteUserServiceModal").modal("show");
                            break;
                        default:
                            ;
					}
				});
				
			}).always(() => {
				if(typeof callback === "function") {
					callback();
				}
			});
    	}
    }
};