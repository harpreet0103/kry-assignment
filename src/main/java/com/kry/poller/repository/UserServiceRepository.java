package com.kry.poller.repository;

import com.kry.poller.entity.UserEntity;
import com.kry.poller.entity.UserServiceEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserServiceRepository extends JpaRepository<UserServiceEntity, Long> {

    List<UserServiceEntity> findAllByUserAndEnabledIsTrue(UserEntity userEntity);

    List<UserServiceEntity> findAllByEnabled(boolean isEnabled);
}
