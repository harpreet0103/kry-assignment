package com.kry.poller.config;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Description;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

@Configuration
public class WebClientConfig {

    @Value("${poller.connectionTimeoutInSeconds : 10}")
    private int connectionTimeoutInSeconds;

    @Bean
    @Description("user service web client")
    public WebClient restClient(){

        final ReactorClientHttpConnector connector = new ReactorClientHttpConnector(
            HttpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectionTimeoutInSeconds * 1000)
                .doOnConnected(connection ->
                    connection
                        .addHandlerLast(new ReadTimeoutHandler(connectionTimeoutInSeconds))
                        .addHandlerLast(new WriteTimeoutHandler(connectionTimeoutInSeconds))
                )
        );

        return WebClient.builder()
            .clientConnector(connector)
            .build();
    }

}
