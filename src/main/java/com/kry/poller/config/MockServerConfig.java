package com.kry.poller.config;

import java.util.ArrayList;
import java.util.List;
import org.mockserver.configuration.ConfigurationProperties;
import org.mockserver.integration.ClientAndServer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnProperty(name = "poller.mock-server.enabled", havingValue = "true")
public class MockServerConfig {

    @Value("${poller.mock-server.ports}")
    private int[] mockServerPorts;

    @Bean
    List<ClientAndServer> mtnMomoMockServer() {
        ConfigurationProperties.logLevel("OFF");
        ConfigurationProperties.initializationJsonPath("expectations/default_expectations.json");
        final List<ClientAndServer> mockServers = new ArrayList<>();
        for(int mockServerPort : mockServerPorts) {
            final ClientAndServer clientAndServer = ClientAndServer.startClientAndServer(mockServerPort);
            mockServers.add(clientAndServer);
        }
        return mockServers;
    }

}
