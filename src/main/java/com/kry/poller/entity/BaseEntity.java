package com.kry.poller.entity;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "enabled")
    private boolean enabled = true;

    @Column(name = "created_on", updatable = false)
    private LocalDateTime createdOn;

    @Column(name = "last_updated", insertable = false)
    private LocalDateTime lastUpdated;

    @Version
    private Integer version;

    @PrePersist
    void onCreate() {
        this.createdOn = LocalDateTime.now();
    }

    @PreUpdate
    void onPersist() {
        this.lastUpdated = LocalDateTime.now();
    }

}
