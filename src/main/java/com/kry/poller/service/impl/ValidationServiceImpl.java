package com.kry.poller.service.impl;

import static com.kry.poller.service.util.Utils.isValidUrl;

import com.kry.poller.entity.UserServiceEntity;
import com.kry.poller.exceptions.InvalidUrlException;
import com.kry.poller.service.ValidationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
class ValidationServiceImpl implements ValidationService {

    @Override
    public void validate(UserServiceEntity userServiceEntity) {
        validateUrl(userServiceEntity.getUrl());
    }

    private void validateUrl(final String url) {
        boolean isValid = true;
        String errorMessage = "";

        if(StringUtils.isBlank(url)) {
            errorMessage = "Url cannot be empty" ;
            isValid = false;
        } else if(!isValidUrl(url)) {
            errorMessage = "Invalid Url: " + url;
            isValid = false;
        }

        if(!isValid) {
            throw new InvalidUrlException(errorMessage);
        }
    }
}
