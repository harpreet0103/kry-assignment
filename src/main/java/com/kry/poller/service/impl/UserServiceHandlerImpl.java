package com.kry.poller.service.impl;

import com.kry.poller.entity.UserEntity;
import com.kry.poller.entity.UserServiceEntity;
import com.kry.poller.repository.UserRepository;
import com.kry.poller.repository.UserServiceRepository;
import com.kry.poller.service.UserServiceHandler;
import java.util.List;
import java.util.Optional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@AllArgsConstructor
@Service
class UserServiceHandlerImpl implements UserServiceHandler {

    private final UserServiceRepository userServiceRepository;
    private final UserRepository userRepository;

    @Override
    @Transactional
    public List<UserServiceEntity> getAllEnabledUserServices() {
        return this.userServiceRepository.findAllByEnabled(true);
    }

    @Override
    @Transactional
    public List<UserServiceEntity> getAllEnabledUserServices(final UserEntity userEntity) {
        return this.userServiceRepository.findAllByUserAndEnabledIsTrue(userEntity);
    }

    @Override
    @Transactional
    public Optional<UserServiceEntity> findById(final Long id) {
        return userServiceRepository.findById(id);
    }

    @Override
    @Transactional
    public void saveUserService(final UserServiceEntity userServiceEntity) {
        userServiceRepository.save(userServiceEntity);
    }

    @Override
    @Transactional
    public void saveUser(final UserEntity userEntity) {
        userRepository.save(userEntity);
    }

    @Override
    @Transactional
    public void deleteUserService(final UserServiceEntity userServiceEntity) {
        userServiceRepository.delete(userServiceEntity);
    }

    @Override
    @Transactional
    public Optional<UserEntity> findUserById(final Long id) {
        return userRepository.findById(id);
    }

    @Override
    @Transactional
    public Optional<UserEntity> findUserByUsername(final String username) {
        return userRepository.findByUserName(username);
    }
}
