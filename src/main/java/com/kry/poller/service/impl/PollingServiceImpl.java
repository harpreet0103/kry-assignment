package com.kry.poller.service.impl;

import com.kry.poller.dto.PollingResponseDto;
import com.kry.poller.entity.UserServiceEntity;
import com.kry.poller.service.UserServiceHandler;
import java.net.URI;
import java.util.Objects;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Slf4j
@AllArgsConstructor
@Configuration
class PollingServiceImpl {

    private final UserServiceHandler userServiceHandler;
    private final WebClient restClient;

    @Scheduled(fixedDelayString = "${poller.intervalInMilliseconds}")
    public void userServicePollingScheduler() {
        userServiceHandler.getAllEnabledUserServices().forEach( userServiceEntity -> {
            final Mono<PollingResponseDto> pollingResponse = checkStatus(userServiceEntity);
            pollingResponse.subscribe(
                // successful response
                pollingResponseDto -> {
                    if("OK".equals(pollingResponseDto.getStatus())) {
                        handleOkResponse(userServiceEntity);
                    } else if("FAIL".equals(pollingResponseDto.getStatus())) {
                        handleFailResponse(userServiceEntity);
                    }
                },
                // failed response
                failedResponse -> {
                    handleFailResponse(userServiceEntity);
                }
            );
        });
    }

    private Mono<PollingResponseDto> checkStatus(final UserServiceEntity userServiceEntity) {
        final URI uri = URI.create(userServiceEntity.getUrl());
        return restClient.get()
            .uri(uri)
            .accept(MediaType.APPLICATION_JSON)
            .exchangeToMono(response -> {
                if (response.statusCode().equals(HttpStatus.OK)) {
                    return response.bodyToMono(PollingResponseDto.class);
                }
                log.error("Status check failed for url" , response.statusCode().value());
                return response.createException().flatMap(Mono::error);
            });
    }

    private void handleFailResponse(final UserServiceEntity userServiceEntity) {
        if("OK".equals(userServiceEntity.getStatus()) || Objects.isNull(userServiceEntity.getStatus())) {
            userServiceEntity.setStatus("FAIL");
            this.userServiceHandler.saveUserService(userServiceEntity);
        }
    }

    private void handleOkResponse(final UserServiceEntity userServiceEntity) {
        if("FAIL".equals(userServiceEntity.getStatus()) || Objects.isNull(userServiceEntity.getStatus())) {
            userServiceEntity.setStatus("OK");
            this.userServiceHandler.saveUserService(userServiceEntity);
        }
    }
}
