package com.kry.poller.service;

import com.kry.poller.entity.UserServiceEntity;

public interface ValidationService {

    void validate(UserServiceEntity userServiceEntity);

}
