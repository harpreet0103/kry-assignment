package com.kry.poller.service;

import com.kry.poller.entity.UserEntity;
import com.kry.poller.entity.UserServiceEntity;
import java.util.List;
import java.util.Optional;

public interface UserServiceHandler {

    List<UserServiceEntity> getAllEnabledUserServices();

    List<UserServiceEntity> getAllEnabledUserServices(UserEntity userEntity);

    Optional<UserServiceEntity> findById(Long id);

    void saveUserService(UserServiceEntity userServiceEntity);

    void saveUser(UserEntity userEntity);

    void deleteUserService(UserServiceEntity userServiceEntity);

    Optional<UserEntity> findUserById(Long id);

    Optional<UserEntity> findUserByUsername(String username);
}
