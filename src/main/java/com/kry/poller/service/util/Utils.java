package com.kry.poller.service.util;

import org.apache.commons.validator.routines.UrlValidator;

public class Utils {

    private static final String[] SCHEMES = {"http","https"};
    private static final UrlValidator URL_VALIDATOR = new UrlValidator(SCHEMES, UrlValidator.ALLOW_LOCAL_URLS);

    public static boolean isValidUrl(String url) {
        return URL_VALIDATOR.isValid(url);
    }

}
