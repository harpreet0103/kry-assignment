package com.kry.poller.controller;

import com.kry.poller.entity.UserEntity;
import com.kry.poller.entity.UserServiceEntity;
import com.kry.poller.exceptions.ResourceNotFoundException;
import com.kry.poller.service.UserServiceHandler;
import com.kry.poller.service.ValidationService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
@RequestMapping("/user-service")
public class UserServiceController {

    private final UserServiceHandler userServiceHandler;
    private final ValidationService validationService;

    @GetMapping("/list")
    public List<UserServiceEntity> getUserServices()
    {
        final UserEntity user = getUser();
        return userServiceHandler.getAllEnabledUserServices(user);
    }

    @PostMapping("/")
    public UserServiceEntity addUserService(@RequestBody UserServiceEntity userServiceEntity) {
        final UserEntity user = getUser();
        userServiceEntity.setUser(user);
        validationService.validate(userServiceEntity);
        userServiceHandler.saveUserService(userServiceEntity);
        return userServiceEntity;
    }

    @PutMapping("/{id}")
    public UserServiceEntity updateUserService(@PathVariable(value = "id") Long serviceId, @RequestBody UserServiceEntity userService) {
        final UserServiceEntity userServiceEntity = userServiceHandler.findById(serviceId)
            .orElseThrow(() -> new ResourceNotFoundException("Service not found for this id :: " + serviceId));
        userServiceEntity.setName(userService.getName());
        userServiceEntity.setUrl(userService.getUrl());
        validationService.validate(userServiceEntity);
        userServiceHandler.saveUserService(userServiceEntity);
        return userServiceEntity;
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteService(@PathVariable(value = "id") Long serviceId) {
        final UserServiceEntity userServiceEntity = userServiceHandler.findById(serviceId)
            .orElseThrow(() -> new ResourceNotFoundException("Service not found for this id :: " + serviceId));
        Map<String, Boolean> response = new HashMap<>();
        userServiceHandler.deleteUserService(userServiceEntity);
        response.put("deleted", true);
        return response;
    }

    private UserEntity getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            final UserDetails userPrincipal = (UserDetails) authentication.getPrincipal();
            final String username = userPrincipal.getUsername();
            return userServiceHandler.findUserByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for " + username));
        }
        throw new ResourceNotFoundException("Unable to fetch logged in user");
    }

}
