package com.kry.poller.dto;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Value;

@AllArgsConstructor
@Value
public class ErrorDetailsDto {

	private LocalDateTime timestamp;
	private String message;
	private String details;

}
