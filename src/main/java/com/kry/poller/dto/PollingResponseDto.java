package com.kry.poller.dto;

import lombok.Data;

@Data
public class PollingResponseDto {
    private String status;
}
