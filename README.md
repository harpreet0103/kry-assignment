Kry Assignment (POLLER)
======================

Description
-----------
A Simple service poller that keeps a list of services defined by a URL, and periodically does a HTTP GET to each and
saves the response ("OK" or "FAIL"). Apart from the polling logic, services can be visualised and easily managed in a basic UI presenting
the all services together with their status.

Technology Stack
----------------
- Spring boot webflux (reactive)
- Spring boot web
- Spring boot security
- Spring boot data jpa
- Java 11
- H2 database (file)
- Lombok
- Mockserver
- JS libraries (Bootstrap, jquery, datatables)

Prerequisites
-------------
- Java 11.


Execution
---------
A jar has been attached in the root directory with name `poller.jar` which can be executed using command

> java -jar poller.jar [--server-port p]

The argument `--server-port` is optional where `p` is the port, if this argument is not present, the default port is 9090, which is also configurable and can be configured in application.properties.

The url to access the web app is:

> [http://localhost:9090/poller](http://localhost:9090/poller)


Accessing Database
------------------

Database can be accessed from browser using url 
`http://localhost:9090/poller/h2-console`

Properties to login to db :

- `Driver Class: org.h2.Driver`
- `Jdbc url: jdbc:h2:file:./database/poller`
- `User name: root`
- `Password: password`


Build using gradle
------------------
`> ./gradlew clean build --refresh-dependencies`


Setup and Testing
----------------
The status check for services can be setup and verified using mock server that can be configured in application.yml.
Mockserver can be setup with multiple ports as :
```
poller:
  mock-server:
    enabled: true
    ports: 20001, 20002
```
The response is configured in `default_expectations.json`. Current configurations are as follow:
 
 - http://localhost:20001/check-status-stable : will result in OK response
 - http://localhost:20001/check-status-unstable : will result in FAIL response
 - http://localhost:20001/check-status-timeout : will result in OK for 30 seconds and FAIL after that
 
 
 
This poller expects the services to be checked should be returning the response 

If service is ok
```
{
    "status": "OK"
}
```

If service is failing (or for Http status other than 200)

```
{
    "status": "FAIL"
}
```

Users
-----
There are three users to login with usernames as:

 - tom
 - jerry
 - pluto
 
All three users have same password i.e. `password`